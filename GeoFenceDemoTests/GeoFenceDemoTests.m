//
//  GeoFenceDemoTests.m
//  GeoFenceDemoTests
//
//  Created by Kirk Hsu on 2018/5/29.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FirebaseDBManager.h"

@interface GeoFenceDemoTests : XCTestCase

@end

@implementation GeoFenceDemoTests



- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSLog(@"setUp");
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    NSLog(@"tearDown");
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSLog(@"testExample");
}

-(void)testGetGeoFenceData{
    XCTestExpectation *dataException = [self expectationWithDescription:@"DataError"];
    
    [FirebaseDBManager.sharedInstance getGeoFenceDataWithCompletionHandler:^(NSArray *array) {
        XCTAssertGreaterThan([array count], 0);
        
        //拋出錯誤
        [dataException fulfill];
    }];
    
    //延遲兩秒執行
    [self waitForExpectationsWithTimeout:2 handler:nil];
}

-(void)testGetHeadlines{
    XCTestExpectation *dataException = [self expectationWithDescription:@"DataError"];
    
    [FirebaseDBManager.sharedInstance getHeadlinesWithCompletionHandler:^(NSArray *array) {
        XCTAssertGreaterThan([array count], 0);
        
        //拋出錯誤
        [dataException fulfill];
    }];
    
    //延遲兩秒執行
    [self waitForExpectationsWithTimeout:2 handler:nil];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
