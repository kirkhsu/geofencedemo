//
//  FirebaseCMManager.m
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/29.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import "FirebaseCMManager.h"
@import Firebase;

@interface FirebaseCMManager()

@end

@implementation FirebaseCMManager

+ (instancetype)sharedInstance{
    static FirebaseCMManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[FirebaseCMManager alloc] init];
    });
    return instance;
}

-(instancetype)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

-(void)sendMessage:(NSString *)msg{
    [[FIRMessaging messaging] sendMessage:@{@"body":msg} to:@"dsex1XFI3u0:APA91bGDOo4sBp77um_PYlOsggKAUcJuYORl85j7C6DRSUFB4csLLMRVkmy7IIoYwaT9r66l_NDRAnUWE0Q2axWbmmOSwMNOBtjAmcci05VJx8zWYLtCM5bkDCp1fWR4hnOWOSwduTq6" withMessageID:@"1" timeToLive:10];
}

@end
