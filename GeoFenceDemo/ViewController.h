//
//  ViewController.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/22.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<CoreLocation/CoreLocation.h>
#import "MapKit/MapKit.h"

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property (nonatomic,strong) CLLocationManager *locationManager;

@end

