//
//  FirebaseDBManager.m
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/23.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import "FirebaseDBManager.h"
@import Firebase;

@interface FirebaseDBManager()
@property (strong, nonatomic) FIRDatabaseReference *ref;
@end

@implementation FirebaseDBManager

+ (instancetype)sharedInstance{
    static FirebaseDBManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[FirebaseDBManager alloc] init];
    });
    return instance;
}

-(instancetype)init{
    self = [super init];
    if(self){
        self.ref = [[FIRDatabase database] reference];
    }
    return self;
}

-(void)getGeoFenceDataWithCompletionHandler:(void (^)(NSArray *array)) completionHandler{
    
    FIRDatabaseQuery *query = [[[self.ref child:@"genfence"]
                                queryOrderedByChild:@"hidden"] queryEqualToValue:@NO];
    
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != (id)[NSNull null]){
            if ([snapshot.value isKindOfClass:[NSArray class]]){
                NSArray *resultArray = (NSArray *)snapshot.value;
                // 過濾掉array中"null"的資料
                NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(id str, NSDictionary *unused) {
                    return str != [NSNull null];
                }];
                NSArray *filteredArray = [resultArray filteredArrayUsingPredicate:pred];

                //NSLog(@"genfence[%@]", filteredArray);
                completionHandler(filteredArray);
            }else if([snapshot.value isKindOfClass:[NSDictionary class]]){
                NSDictionary *resultDic = snapshot.value;
                NSArray *valueArray = [resultDic allValues];
                //NSLog(@"headlines[%@]", valueArray);
                completionHandler(valueArray);
            }
        }
    }];
}

-(void)getHeadlinesWithCompletionHandler:(void (^)(NSArray *array)) completionHandler{
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    FIRDatabaseQuery *query = [[[self.ref child:@"headlines"]
                                queryOrderedByChild:@"appid"] queryEqualToValue:bundleIdentifier];
    
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if(snapshot.value != (id)[NSNull null]){
            if ([snapshot.value isKindOfClass:[NSArray class]]){
                NSArray *resultArray = (NSArray *)snapshot.value;
                // 過濾掉array中"null"的資料
                NSPredicate *pred = [NSPredicate predicateWithBlock:^BOOL(id str, NSDictionary *unused) {
                    return str != [NSNull null];
                }];
                NSArray *filteredArray = [resultArray filteredArrayUsingPredicate:pred];
                
                //NSLog(@"headlines[%@]", filteredArray);
                completionHandler(filteredArray);
            }else if ([snapshot.value isKindOfClass:[NSDictionary class]]){
                NSDictionary *resultDic = snapshot.value;
                NSArray *valueArray = [resultDic allValues];
                //NSLog(@"headlines[%@]", valueArray);
                completionHandler(valueArray);
            }
        }
    }];
}

@end
