//
//  FirebaseCMManager.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/29.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FirebaseCMManager : NSObject

+ (instancetype)sharedInstance;

-(void)sendMessage:(NSString *)msg;

@end
