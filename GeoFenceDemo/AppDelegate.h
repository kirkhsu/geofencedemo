//
//  AppDelegate.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/22.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

