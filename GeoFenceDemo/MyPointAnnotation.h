//
//  MyPointAnnotation.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/6/1.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyPointAnnotation : MKPointAnnotation

@property (nonatomic, strong) NSString *placeId;
@property (nonatomic, strong) NSString *desc;

@end
