//
//  MyRegion.m
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/25.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import "MyRegion.h"

@implementation MyRegion
-(BOOL)notifyOnEntry{
    NSLog(@"notifyOnEntry");
    return NO;
}
-(BOOL)notifyOnExit{
    NSLog(@"notifyOnExit");
    return NO;
}
@end
