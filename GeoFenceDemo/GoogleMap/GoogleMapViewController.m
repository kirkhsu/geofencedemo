//
//  GoogleMapViewController.m
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/25.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import "GoogleMapViewController.h"

@interface GoogleMapViewController () <CLLocationManagerDelegate>

@end

@implementation GoogleMapViewController

NSMutableArray *likelyPlaces;
GMSPlace *selectedPlace;
CLLocation *defaultLocation;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestAlwaysAuthorization];
    self.locationManager.distanceFilter = 50;
    [self.locationManager startUpdatingLocation];
    self.locationManager.delegate = self;
    
    likelyPlaces = [[NSMutableArray alloc] init];
    self.placesClient = [GMSPlacesClient sharedClient];
    self.zoomLevel = 15.0;
    defaultLocation = [[CLLocation alloc] initWithLatitude:25.062417 longitude:121.542338];
    
    // Create a GMSCameraPosition that tells the map to display the
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:defaultLocation.coordinate.latitude longitude:defaultLocation.coordinate.longitude zoom:self.zoomLevel];
    
    self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view = self.mapView;
//    [self.view addSubview:mapView];
    
//    self.mapView.mapType = kGMSTypeHybrid;
//    self.mapView.accessibilityElementsHidden = NO;
    
//    [mapView setHidden:YES];
    
    // Creates a marker in the center of the map.
//    GMSMarker *marker = [[GMSMarker alloc] init];
//    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
//    marker.title = @"Sydney";
//    marker.snippet = @"Australia";
//    marker.map = mapView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - private method
-(void)showAlertWithMessage:(NSString *)msg{
    UIAlertController *alertOne = [UIAlertController alertControllerWithTitle:@"系統通知" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:nil];
    [alertOne addAction:confirmAction];
    // 彈出alertOne
    [self presentViewController:alertOne animated:YES completion:nil];
}

-(void)listLikelyPlaces{
    [likelyPlaces removeAllObjects];
    [self.placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList * _Nullable likelihoodList, NSError * _Nullable error) {
        if(error){
            NSLog(@"Current Place error: %@", error.localizedDescription);
        }
//        NSLog(@"place count %lu", [likelihoodList.likelihoods count]);
        for(GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods){
            GMSPlace *place = likelihood.place;
//            NSLog(@"place name [%@]", [place name]);
            [likelyPlaces addObject:place];
        }
    }];
}

#pragma mark - CLLocationManagerDelegate

/*
 *  locationManager:didUpdateLocations:
 *
 *  Discussion:
 *    Invoked when new locations are available.  Required for delivery of
 *    deferred locations.  If implemented, updates will
 *    not be delivered to locationManager:didUpdateToLocation:fromLocation:
 *
 *    locations is an array of CLLocation objects in chronological order.
 */
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *location = [locations lastObject];
    NSLog(@"Update locations %f, %f", location.coordinate.latitude, location.coordinate.longitude);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:self.zoomLevel];
    
    if([self.mapView isHidden]){
        [self.mapView setHidden:NO];
        [self.mapView setCamera:camera];
    }else{
        [self.mapView animateToCameraPosition:camera];
    }
    
//    [self listLikelyPlaces];
}

/*
 *  locationManager:didUpdateHeading:
 *
 *  Discussion:
 *    Invoked when a new heading is available.
 */
- (void)locationManager:(CLLocationManager *)manager
       didUpdateHeading:(CLHeading *)newHeading
{
    
}

/*
 *  locationManagerShouldDisplayHeadingCalibration:
 *
 *  Discussion:
 *    Invoked when a new heading is available. Return YES to display heading calibration info. The display
 *    will remain until heading is calibrated, unless dismissed early via dismissHeadingCalibrationDisplay.
 */
- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;
}

/*
 *  locationManager:didDetermineState:forRegion:
 *
 *  Discussion:
 *    Invoked when there's a state transition for a monitored region or in response to a request for state via a
 *    a call to requestStateForRegion:.
 */
- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    switch (state) {
        case CLRegionStateUnknown:
            NSLog(@"未知狀態");
            break;
        case CLRegionStateInside:
            NSLog(@"在區域內部");
            break;
        case CLRegionStateOutside:
            NSLog(@"在區域外部");
            break;
        default:
            break;
    }
}

/*
 *  locationManager:didRangeBeacons:inRegion:
 *
 *  Discussion:
 *    Invoked when a new set of beacons are available in the specified region.
 *    beacons is an array of CLBeacon objects.
 *    If beacons is empty, it may be assumed no beacons that match the specified region are nearby.
 *    Similarly if a specific beacon no longer appears in beacons, it may be assumed the beacon is no longer received
 *    by the device.
 */
- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region
{
    
}

/*
 *  locationManager:rangingBeaconsDidFailForRegion:withError:
 *
 *  Discussion:
 *    Invoked when an error has occurred ranging beacons in a region. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
              withError:(NSError *)error
{
    
}

/*
 *  locationManager:didEnterRegion:
 *
 *  Discussion:
 *    Invoked when the user enters a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 *  進去監聽區域後調用（調用一次）
 */
- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region
{
    NSLog(@"已進入-%@", region.identifier);
    [self showAlertWithMessage:[NSString stringWithFormat:@"已進入-%@", region.identifier]];

    //    [manager stopMonitoringForRegion:region];
}

/*
 *  locationManager:didExitRegion:
 *
 *  Discussion:
 *    Invoked when the user exits a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 *  離開監聽區域後調用（調用一次）
 */
- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region
{
    NSLog(@"已離開-%@", region.identifier);
    [self showAlertWithMessage:[NSString stringWithFormat:@"已離開-%@", region.identifier]];
}

/*
 *  locationManager:didFailWithError:
 *
 *  Discussion:
 *    Invoked when an error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

/*
 *  locationManager:monitoringDidFailForRegion:withError:
 *
 *  Discussion:
 *    Invoked when a region monitoring error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(nullable CLRegion *)region
              withError:(NSError *)error
{
    NSLog(@"monitoringDidFailForRegion: %@ withError: %@", region, error);
}

/*
 *  locationManager:didChangeAuthorizationStatus:
 *
 *  Discussion:
 *    Invoked when the authorization status changes for this application.
 *    监听用户授权状态
 */
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: // 用戶還未決定
            NSLog(@"用戶還未決定");
            break;
        case kCLAuthorizationStatusRestricted: // 訪問受限(蘋果預留選項,暫時沒用)
            NSLog(@"訪問受限");
            break;
        case kCLAuthorizationStatusDenied: // 定位關閉時和對此APP授權爲never時調用
            // 定位是否可用（是否支持定位或者定位是否開啓）
            if([CLLocationManager locationServicesEnabled]){
                NSLog(@"定位開啓，但被拒");
                // 在此處, 應該提醒用戶給此應用授權, 並跳轉到"設置"界面讓用戶進行授權
                // 在iOS8.0之後跳轉到"設置"界面代碼
                NSURL *settingURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if([[UIApplication sharedApplication] canOpenURL:settingURL]){
                    [[UIApplication sharedApplication] openURL:settingURL options:@{} completionHandler:nil];
                }
            }else{
                NSLog(@"定位關閉，不可用");
            }
            [self.mapView setHidden:NO];
            break;
        case kCLAuthorizationStatusAuthorizedAlways: // 獲取前後臺定位授權
            NSLog(@"獲取前後臺定位授權");
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse: // 獲得前臺定位授權
            NSLog(@"獲得前臺定位授權");
            break;
        default:
            break;
    }
}

/*
 *  locationManager:didStartMonitoringForRegion:
 *
 *  Discussion:
 *    Invoked when a monitoring for a region started successfully.
 */
- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region
{
    
}

/*
 *  Discussion:
 *    Invoked when location updates are automatically paused.
 */
- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    
}

/*
 *  Discussion:
 *    Invoked when location updates are automatically resumed.
 *
 *    In the event that your application is terminated while suspended, you will
 *      not receive this notification.
 */
- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    
}

/*
 *  locationManager:didFinishDeferredUpdatesWithError:
 *
 *  Discussion:
 *    Invoked when deferred updates will no longer be delivered. Stopping
 *    location, disallowing deferred updates, and meeting a specified criterion
 *    are all possible reasons for finishing deferred updates.
 *
 *    An error will be returned if deferred updates end before the specified
 *    criteria are met (see CLError), otherwise error will be nil.
 */
- (void)locationManager:(CLLocationManager *)manager
didFinishDeferredUpdatesWithError:(nullable NSError *)error
{
    
}

/*
 *  locationManager:didVisit:
 *
 *  Discussion:
 *    Invoked when the CLLocationManager determines that the device has visited
 *    a location, if visit monitoring is currently started (possibly from a
 *    prior launch).
 */
- (void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit
{
    
}

@end
