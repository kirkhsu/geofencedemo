//
//  GoogleMapViewController.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/25.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<CoreLocation/CoreLocation.h>
@import GoogleMaps;
@import GooglePlaces;

@interface GoogleMapViewController : UIViewController

@property (nonatomic,strong) CLLocationManager *locationManager;

@property (nonatomic,strong) GMSMapView *mapView;
@property (nonatomic,strong) GMSPlacesClient *placesClient;

@property (nonatomic) float zoomLevel;

@end
