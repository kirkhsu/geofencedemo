//
//  MyAnnotationView.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/6/1.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyAnnotationView : MKPinAnnotationView

@property (nonatomic, strong) NSString *placeId;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *customTitleLabel;
@property (nonatomic, strong) UILabel *customDescLabel;

@property (nonatomic, strong) UIView *customView;

-(void)showCustomView;
-(void)hideCustomView;

@end
