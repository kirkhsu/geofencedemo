//
//  MyRegion.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/25.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface MyRegion : CLRegion

@end
