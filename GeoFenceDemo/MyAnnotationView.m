//
//  MyAnnotationView.m
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/6/1.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import "MyAnnotationView.h"
#import "Masonry.h"

@implementation MyAnnotationView

-(void)showCustomView{
    if(self.customView){
        [self.customView setHidden:NO];
    }else{
        NSLog(@"customView is nil");
    }
}

-(void)hideCustomView{
    [self.customView setHidden:YES];
}

-(UILabel *)customTitleLabel{
    if(_customTitleLabel == nil){
        _customTitleLabel = [[UILabel alloc] init];
        [_customTitleLabel setTextColor:[UIColor darkGrayColor]];
        [_customTitleLabel setMinimumScaleFactor:0.5];
        [_customTitleLabel setAdjustsFontSizeToFitWidth:YES];
        [self.customView addSubview:_customTitleLabel];
        [_customTitleLabel setTextAlignment:(NSTextAlignmentLeft)];
        [_customTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(5);
            make.left.mas_equalTo(5);
            make.right.mas_equalTo(-5);
            make.height.mas_equalTo(30);
        }];
    }
    return _customTitleLabel;
}

-(UILabel *)customDescLabel{
    if(_customDescLabel== nil){
        _customDescLabel = [[UILabel alloc] init];
        [_customDescLabel setTextColor:[UIColor grayColor]];
        [self.customView addSubview:_customDescLabel];
        [_customDescLabel setNumberOfLines:2];
        [_customDescLabel setAdjustsFontSizeToFitWidth:YES];
        [_customDescLabel setMinimumScaleFactor:0.5];
        [_customDescLabel setTextAlignment:(NSTextAlignmentLeft)];
        [_customDescLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.customTitleLabel.mas_bottom).mas_offset(2);
            make.left.mas_equalTo(5);
            make.right.mas_equalTo(-5);
            make.bottom.mas_equalTo(-5);
        }];
    }
    return _customDescLabel;
}

-(UIView *)customView{
    if(_customView == nil){
        _customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 70)];
        _customView.layer.cornerRadius = 6.0;
        _customView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_customView];
        [_customView setHidden:YES];

        _customView.center = CGPointMake(_customView.bounds.size.width*0.1f, -_customView.bounds.size.height*0.5f-5);
    }
    return _customView;
}

-(UILabel *)titleLabel{
    if(_titleLabel == nil){
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
        [_titleLabel setTextColor:[UIColor darkGrayColor]];
        [_titleLabel setTextAlignment:(NSTextAlignmentCenter)];
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:13]];
        [self addSubview:_titleLabel];
        
        _titleLabel.center = CGPointMake(_titleLabel.bounds.size.width*0.1f, _titleLabel.bounds.size.height+5.0f);
    }
    return _titleLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
