//
//  ViewController.m
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/22.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import "ViewController.h"
#import "FirebaseDBManager.h"
#import "FirebaseCMManager.h"
@import UserNotifications;
#import "MyAnnotationView.h"
#import "Masonry.h"
#import "MyPointAnnotation.h"

@interface ViewController () <CLLocationManagerDelegate, MKMapViewDelegate>

@end

@implementation ViewController

NSTimer *timer;
NSArray *placeArray;
NSArray *headlineArray;
NSMutableArray *regionArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // 每隔多少米定位一次
    // 功能: 只有當最新的位置與上一次獲取的位置之間的距離, 大於這個值時, 纔會通過代理告訴外界.
    self.locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
    // 設置定位精確度
    // 通過設置此屬性, 獲取不同精確度的位置信息
    // 精確度越高，越耗電，定位所需時間越長
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MKUserTrackingModeFollowWithHeading;

    regionArray = [[NSMutableArray alloc] init];
    
    CLLocationCoordinate2D noLocation = CLLocationCoordinate2DMake(25.063404, 121.545051);
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 1000, 1000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:YES];
    
    long monitoredRegionsCount = [self.locationManager.monitoredRegions count];
    NSLog(@"monitoredRegionsCount=%ld", monitoredRegionsCount);
    if(monitoredRegionsCount > 0){
        for(CLCircularRegion *region in self.locationManager.monitoredRegions){
            [self.locationManager stopMonitoringForRegion:region];
        }
    }

    [FirebaseDBManager.sharedInstance getGeoFenceDataWithCompletionHandler:^(NSArray *array) {
        placeArray = array;
        for(NSDictionary *data in array){
            [self setupRegionWithData:data];
        }
    }];
    
    [FirebaseDBManager.sharedInstance getHeadlinesWithCompletionHandler:^(NSArray *array) {
        headlineArray = array;
    }];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(CLLocationManager.authorizationStatus == kCLAuthorizationStatusNotDetermined){
        [self.locationManager requestAlwaysAuthorization]; // 後臺定位
        //[self.locationManager requestWhenInUseAuthorization]; // 前臺定位
    }
    
    if(CLLocationManager.authorizationStatus == kCLAuthorizationStatusDenied){
        NSLog(@"Warning!! Get Location Denied");
    }
    
    if(CLLocationManager.authorizationStatus == kCLAuthorizationStatusAuthorizedAlways){
        [self.locationManager startUpdatingLocation];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private method

-(void)setupRegionWithData:(NSDictionary *)data{
    NSString *title = [data valueForKey:@"title"];
    NSString *latitude = [data valueForKeyPath:@"coordinate.latitude"];
    double latDouble = [latitude doubleValue];
    NSString *longitude = [data valueForKeyPath:@"coordinate.longitude"];
    double lonDouble = [longitude doubleValue];
    NSString *radius = [data valueForKey:@"radius"];
    double radDouble = [radius doubleValue];
    NSString *placeid = [NSString stringWithFormat:@"%@", [data valueForKey:@"placeid"]];
    
    // 檢查系統是否能夠監視 region
    if ([CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
        
        // 創建區域中心
        CLLocationCoordinate2D center = CLLocationCoordinate2DMake(latDouble, lonDouble);
        
        // 創建大頭釘(annotation)
        MyPointAnnotation *pointAnnotaion = [[MyPointAnnotation alloc] init];
        pointAnnotaion.coordinate = center;
        pointAnnotaion.title = title;
        pointAnnotaion.placeId = placeid;
        [self.mapView addAnnotation:pointAnnotaion];
        
        // 創建區域（指定區域中心，和區域半徑）
        CLLocationDistance radius = radDouble; // 單位米
        
        NSLog(@"最大監聽區域半徑 %f", self.locationManager.maximumRegionMonitoringDistance);
        // 判斷區域半徑是否大於最大監聽區域半徑,如果大於, 就沒法監聽
        if (radius > self.locationManager.maximumRegionMonitoringDistance) {
            radius = self.locationManager.maximumRegionMonitoringDistance;
            NSLog(@"重設區域半徑 %f", radius);
        }
        
        CLCircularRegion *region = [[CLCircularRegion alloc] initWithCenter:center radius:radius identifier:placeid];
        
        // 開始監聽指定區域
        [self.locationManager startMonitoringForRegion:region];
        
//        long rangedRegionsCount = [self.locationManager.rangedRegions count];
//        NSLog(@"rangedRegionsCount=%ld", rangedRegionsCount); // for beacon
        
        [regionArray addObject:region];
        
        // 獲取某個區域的當前狀態
        [self.locationManager requestStateForRegion:region];
        
        if(timer == nil){
            __weak typeof(self) weakSelf = self;
            timer = [NSTimer scheduledTimerWithTimeInterval:10.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                // 獲取某個區域的當前狀態
                // [weakSelf.locationManager requestStateForRegion:region];
                for(CLRegion *region in regionArray){
                    [weakSelf.locationManager requestStateForRegion:region];
                }
            }];
        }
        
        // 繪製一個圓圈圖形（用於表示 region 的範圍）
        MKCircle *circle = [MKCircle circleWithCenterCoordinate:center radius:radius];
        [self.mapView addOverlay:circle];
        
        // 繪製多邊形
/*
        CLLocationCoordinate2D coordinates[5];
        coordinates[0].latitude = 25.061868;
        coordinates[0].longitude = 121.547404;
        
        coordinates[1].latitude = 25.061698;
        coordinates[1].longitude = 121.547415;
        
        coordinates[2].latitude = 25.061552;
        coordinates[2].longitude = 121.546747;
        
        coordinates[3].latitude = 25.061875;
        coordinates[3].longitude = 121.547050;
        
        coordinates[4].latitude = 25.061868;
        coordinates[4].longitude = 121.547404;
        
        MKPolygon *polygon = [MKPolygon polygonWithCoordinates:coordinates count:5];
        [self.mapView addOverlay:polygon];
*/
    }else{
        NSLog(@"區域監聽不可用");
        [self setMessage:@"區域監聽不可用"];
    }
    
}

-(void)setMessage:(NSString *)message{
    self.messageLabel.text = message;
}

-(void)showAlertWithMessage:(NSString *)msg{
    UIAlertController *alertOne = [UIAlertController alertControllerWithTitle:@"系統通知" message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:nil];
    [alertOne addAction:confirmAction];
    // 彈出alertOne
    [self presentViewController:alertOne animated:YES completion:nil];
}

-(void)showLocalNotificationWithMessage:(NSString *)msg subtitle:(NSString *)subtitle{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge|UNAuthorizationOptionSound|UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        // A Boolean value indicating whether authorization was granted. The value of this parameter is YES when authorization for the requested options was granted. The value is NO when authorization for one or more of the options is denied.
        if (granted) {
            
            // 1、創建通知內容，註：這裡得用可變類型的UNMutableNotificationContent，否則內容的屬性是只讀的
            UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
            // 標題
            content.title = @"System Push";
            // 次標題
            content.subtitle = subtitle;
            // 內容
            content.body = msg;
            
//            self.badge++;
            // app顯示通知數量的角標
            content.badge = @(1);
            
            // 通知的提示聲音，這裡用的默認的聲音
            content.sound = [UNNotificationSound defaultSound];
            
//            NSURL *imageUrl = [[NSBundle mainBundle] URLForResource:@"jianglai" withExtension:@"jpg"];
//            UNNotificationAttachment *attachment = [UNNotificationAttachment attachmentWithIdentifier:@"imageIndetifier" URL:imageUrl options:nil error:nil];
            
            // 附件 可以是音頻、圖片、視頻 這裡是一張圖片
//            content.attachments = @[attachment];
            
            // 標識符
            content.categoryIdentifier = @"categoryIndentifier";
            
            // 2、創建通知觸發
            /* 觸發器分三種：
             UNTimeIntervalNotificationTrigger : 在一定時間後觸發，如果設置重複的話，timeInterval不能小於60
             UNCalendarNotificationTrigger : 在某天某時觸發，可重複
             UNLocationNotificationTrigger : 進入或離開某個地理區域時觸發
             */
            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
            
            // 3、創建通知請求
            UNNotificationRequest *notificationRequest = [UNNotificationRequest requestWithIdentifier:@"KFGroupNotification" content:content trigger:trigger];
            
            // 4、將請求加入通知中心
            [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:notificationRequest withCompletionHandler:^(NSError * _Nullable error) {
                if (error == nil) {
                    NSLog(@"已成功加推送%@",notificationRequest.identifier);
                }
            }];
        }

    }];
    
    
    
    
//    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
//    content.title = [NSString localizedUserNotificationStringForKey:@"System Message" arguments:nil];
//    content.body = [NSString localizedUserNotificationStringForKey:msg
//                                                         arguments:nil];

    // Configure the trigger for a 7am wakeup.
//    NSDateComponents* date = [[NSDateComponents alloc] init];
//    date.hour = 7;
//    date.minute = 0;
//    UNCalendarNotificationTrigger* trigger = [UNCalendarNotificationTrigger
//                                              triggerWithDateMatchingComponents:date repeats:NO];

    // Create the request object.
//    UNNotificationRequest* request = [UNNotificationRequest
//                                      requestWithIdentifier:@"MorningAlarm" content:content trigger:trigger];
    
}

#pragma mark - CLLocationManagerDelegate

/*
 *  locationManager:didUpdateLocations:
 *
 *  Discussion:
 *    Invoked when new locations are available.  Required for delivery of
 *    deferred locations.  If implemented, updates will
 *    not be delivered to locationManager:didUpdateToLocation:fromLocation:
 *
 *    locations is an array of CLLocation objects in chronological order.
 */
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
//    CLLocation *location = locations[0];
//    NSLog(@"Update locations %f, %f", location.coordinate.latitude, location.coordinate.longitude);
//    [self setMessage:[NSString stringWithFormat:@"Update locations %f, %f", location.coordinate.latitude, location.coordinate.longitude]];
}

/*
 *  locationManager:didUpdateHeading:
 *
 *  Discussion:
 *    Invoked when a new heading is available.
 */
- (void)locationManager:(CLLocationManager *)manager
       didUpdateHeading:(CLHeading *)newHeading
{
    
}

/*
 *  locationManagerShouldDisplayHeadingCalibration:
 *
 *  Discussion:
 *    Invoked when a new heading is available. Return YES to display heading calibration info. The display
 *    will remain until heading is calibrated, unless dismissed early via dismissHeadingCalibrationDisplay.
 */
- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;
}

/*
 *  locationManager:didDetermineState:forRegion:
 *
 *  Discussion:
 *    Invoked when there's a state transition for a monitored region or in response to a request for state via a
 *    a call to requestStateForRegion:.
 */
- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    switch (state) {
        case CLRegionStateUnknown:
            //NSLog(@"未知狀態 (region.id=%@)", region.identifier);
            [self setMessage:[NSString stringWithFormat:@"未知狀態 (region.id=%@)", region.identifier]];
            break;
        case CLRegionStateInside:
            //NSLog(@"在區域內部 (region.id=%@)", region.identifier);
            [self setMessage:[NSString stringWithFormat:@"在區域內部 (region.id=%@)", region.identifier]];
            break;
        case CLRegionStateOutside:
            //NSLog(@"在區域外部 (region.id=%@)", region.identifier);
            [self setMessage:[NSString stringWithFormat:@"在區域外部 (region.id=%@)", region.identifier]];
            break;
        default:
            break;
    } 
}

/*
 *  locationManager:didRangeBeacons:inRegion:
 *
 *  Discussion:
 *    Invoked when a new set of beacons are available in the specified region.
 *    beacons is an array of CLBeacon objects.
 *    If beacons is empty, it may be assumed no beacons that match the specified region are nearby.
 *    Similarly if a specific beacon no longer appears in beacons, it may be assumed the beacon is no longer received
 *    by the device.
 */
- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region
{
    
}

/*
 *  locationManager:rangingBeaconsDidFailForRegion:withError:
 *
 *  Discussion:
 *    Invoked when an error has occurred ranging beacons in a region. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
              withError:(NSError *)error
{
    
}

/*
 *  locationManager:didEnterRegion:
 *
 *  Discussion:
 *    Invoked when the user enters a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 *  進去監聽區域後調用（調用一次）
 */
- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region
{
    if(placeArray == nil){
        return;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"placeid = %d", [region.identifier intValue]];
    NSArray *array = [headlineArray filteredArrayUsingPredicate:predicate];
    NSArray *place = [placeArray filteredArrayUsingPredicate:predicate];
    
    NSLog(@"[GF] 已進入-%@", [place[0] valueForKey:@"title"]);
    if([array count] > 0){
        [self showLocalNotificationWithMessage:[array[0] valueForKey:@"content"] subtitle:[place[0] valueForKey:@"title"]];
    }else{
        [self showAlertWithMessage:[NSString stringWithFormat:@"已進入-%@", [place[0] valueForKey:@"title"]]];
    }

//    [manager stopMonitoringForRegion:region];
}

/*
 *  locationManager:didExitRegion:
 *
 *  Discussion:
 *    Invoked when the user exits a monitored region.  This callback will be invoked for every allocated
 *    CLLocationManager instance with a non-nil delegate that implements this method.
 *  離開監聽區域後調用（調用一次）
 */
- (void)locationManager:(CLLocationManager *)manager
          didExitRegion:(CLRegion *)region
{
    if(placeArray == nil){
        return;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"placeid = %d", [region.identifier intValue]];
    NSArray *place = [placeArray filteredArrayUsingPredicate:predicate];
    
    NSLog(@"[GF] 已離開-%@", [place[0] valueForKey:@"title"]);
//    [self showAlertWithMessage:[NSString stringWithFormat:@"已離開-%@", [place[0] valueForKey:@"title"]]];
//    [self showLocalNotificationWithMessage:[NSString stringWithFormat:@"已離開-%@", [place[0] valueForKey:@"title"]] subtitle:[place[0] valueForKey:@"title"]];
}

/*
 *  locationManager:didFailWithError:
 *
 *  Discussion:
 *    Invoked when an error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"[GF] didFailWithError: %@", [error localizedDescription]);
}

/*
 *  locationManager:monitoringDidFailForRegion:withError:
 *
 *  Discussion:
 *    Invoked when a region monitoring error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(nullable CLRegion *)region
              withError:(NSError *)error
{
    NSLog(@"[GF] monitoringDidFailForRegion: %@ withError: %@", region, error);
}

/*
 *  locationManager:didChangeAuthorizationStatus:
 *
 *  Discussion:
 *    Invoked when the authorization status changes for this application.
 *    监听用户授权状态
 */
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: // 用戶還未決定
            NSLog(@"用戶還未決定");
            break;
        case kCLAuthorizationStatusRestricted: // 訪問受限(蘋果預留選項,暫時沒用)
            NSLog(@"訪問受限");
            break;
        case kCLAuthorizationStatusDenied: // 定位關閉時和對此APP授權爲never時調用
            // 定位是否可用（是否支持定位或者定位是否開啓）
            if([CLLocationManager locationServicesEnabled]){
                NSLog(@"定位開啓，但被拒");
                // 在此處, 應該提醒用戶給此應用授權, 並跳轉到"設置"界面讓用戶進行授權
                // 在iOS8.0之後跳轉到"設置"界面代碼
                NSURL *settingURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if([[UIApplication sharedApplication] canOpenURL:settingURL]){
                    [[UIApplication sharedApplication] openURL:settingURL options:@{} completionHandler:nil];
                }
            }else{
                NSLog(@"定位關閉，不可用");
            }
            break;
        case kCLAuthorizationStatusAuthorizedAlways: // 獲取前後臺定位授權
            NSLog(@"獲取前後臺定位授權");
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse: // 獲得前臺定位授權
            NSLog(@"獲得前臺定位授權");
            break;
        default:
            break;
    }
}

/*
 *  locationManager:didStartMonitoringForRegion:
 *
 *  Discussion:
 *    Invoked when a monitoring for a region started successfully.
 */
- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"placeid = %d", [region.identifier intValue]];
    NSArray *place = [placeArray filteredArrayUsingPredicate:predicate];
    
    NSLog(@"[GF] didStartMonitoringForRegion %@ radius %f", [place[0] valueForKey:@"title"], [(CLCircularRegion *)region radius]);
}

/*
 *  Discussion:
 *    Invoked when location updates are automatically paused.
 */
- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    
}

/*
 *  Discussion:
 *    Invoked when location updates are automatically resumed.
 *
 *    In the event that your application is terminated while suspended, you will
 *      not receive this notification.
 */
- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    
}

/*
 *  locationManager:didFinishDeferredUpdatesWithError:
 *
 *  Discussion:
 *    Invoked when deferred updates will no longer be delivered. Stopping
 *    location, disallowing deferred updates, and meeting a specified criterion
 *    are all possible reasons for finishing deferred updates.
 *
 *    An error will be returned if deferred updates end before the specified
 *    criteria are met (see CLError), otherwise error will be nil.
 */
- (void)locationManager:(CLLocationManager *)manager
didFinishDeferredUpdatesWithError:(nullable NSError *)error
{
    
}

/*
 *  locationManager:didVisit:
 *
 *  Discussion:
 *    Invoked when the CLLocationManager determines that the device has visited
 *    a location, if visit monitoring is currently started (possibly from a
 *    prior launch).
 */
- (void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit
{
    
}

#pragma mark - MKMapViewDelegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolygon class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        renderer.strokeColor = [UIColor redColor];
        renderer.fillColor = [UIColor redColor];
        renderer.lineWidth = 1.0;
        return renderer;
    }else if([overlay isKindOfClass:[MKCircle class]]){
        MKCircleRenderer *circleRenderer = [[MKCircleRenderer alloc] initWithOverlay:overlay];
        circleRenderer.strokeColor = [UIColor redColor];
        circleRenderer.lineWidth = 1;
        return circleRenderer;
    }else{
        return nil;
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MyPointAnnotation *pma = (MyPointAnnotation *)annotation;
    MyAnnotationView *annotationView = [[MyAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    [annotationView.customTitleLabel setText:pma.title];
    annotationView.placeId = pma.placeId;
    [annotationView.titleLabel setText:pma.title];
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"calloutAccessoryControlTapped");
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    MyAnnotationView *mv = (MyAnnotationView *)view;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"placeid = %d", [mv.placeId intValue]];
    NSArray *array = [headlineArray filteredArrayUsingPredicate:predicate];
    if(array && [array count] >= 1){
        [mv.customDescLabel setText:[array[0] valueForKey:@"content"]];
    }

    [mv showCustomView];
    
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    MyAnnotationView *mv = (MyAnnotationView *)view;
    [mv hideCustomView];
}

@end
