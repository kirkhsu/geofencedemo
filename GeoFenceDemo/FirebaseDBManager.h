//
//  FirebaseDBManager.h
//  GeoFenceDemo
//
//  Created by Kirk Hsu on 2018/5/23.
//  Copyright © 2018年 KirkHsu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FirebaseDBManager : NSObject

+ (instancetype)sharedInstance;

/// 取得地理圍欄的資訊
-(void)getGeoFenceDataWithCompletionHandler:(void (^)(NSArray *array)) completionHandler;

/// 取得頭條
-(void)getHeadlinesWithCompletionHandler:(void (^)(NSArray *array)) completionHandler;

@end
